package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.html5.LocalStorage;
import org.openqa.selenium.html5.WebStorage;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

import static constants.Environment.*;
import static constants.XPath.*;
import static helpers.WebDriverHelper.getWaitWebDriver;
import static helpers.WebDriverHelper.getWebDriver;

public abstract class LoginPage {
    public static Boolean isLoggedIn = false;
    private static Boolean isInitialised = false;
    private static WebDriver webDriver = null;
    private static WebDriverWait webDriverWait = null;

    public static void setupLoginPage() {
        if (!isInitialised) {
            webDriver = getWebDriver();
            webDriverWait = getWaitWebDriver();
            isInitialised = true;
        }
        if(dev.equalsIgnoreCase("true")){
            webDriver.get(devUrl + loginPageURL);
        } else {
            webDriver.get(prodUrl + loginPageURL);
        }
    }

    public static void setupLoginPage(WebDriver webDriver) {
        if(dev.equalsIgnoreCase("true")){
            webDriver.get(devUrl + loginPageURL);
        } else {
            webDriver.get(prodUrl + loginPageURL);
        }
    }

    public static void navigateToLoginPage(){
        if(dev.equalsIgnoreCase("true")){
            webDriver.get(devUrl + loginPageURL);
        } else {
            webDriver.get(prodUrl + loginPageURL);
        }
    }

    public static void resetLocalStorage(){
        getLocalStorage().clear();
    }

    public static void tearDownLoginPage() {
        webDriver.quit();
    }

    public static void handleLogin(){
        try{
            setUserEmailInput();
            setPasswordInput();
            clickLogin();
            isLoggedIn = true;
        } catch (Exception e){
            isLoggedIn = false;
        }
    }

    public static void handleLogin(WebDriver webDriver){
        try{
            setUserEmailInput(webDriver);
            setPasswordInput(webDriver);
            clickLogin();
            isLoggedIn = true;
        } catch (Exception e){
            isLoggedIn = false;
        }
    }

    public static void handleLogin(String email, String password){
        try{
            setUserEmailInput(email);
            setPasswordInput(password);
            clickLogin();
            isLoggedIn = true;
        } catch (Exception e){
            isLoggedIn = false;
        }
    }

    public static void handleLogout(){
        clickLogout();
        isLoggedIn = false;
    }

    public static LocalStorage getLocalStorage(){
        return ((WebStorage) webDriver).getLocalStorage();
    }

    // Page function

    public static void setUserEmailInput(){
        WebElement userEmailInputField = webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(loginUserEmailInput)));
        userEmailInputField.click();
        userEmailInputField.sendKeys(username);
    }

    public static void setPasswordInput(){
        WebElement passwordInputField = webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(loginUserPasswordInput)));
        passwordInputField.click();
        passwordInputField.sendKeys(password);
    }

    public static void setUserEmailInput(WebDriver webDriver){
        webDriverWait = new WebDriverWait(webDriver, Duration.ofSeconds(10));
        WebElement userEmailInputField = webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(loginUserEmailInput)));
        userEmailInputField.click();
        userEmailInputField.sendKeys(username);
    }

    public static void setPasswordInput(WebDriver webDriver){
        webDriverWait = new WebDriverWait(webDriver, Duration.ofSeconds(10));
        WebElement passwordInputField = webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(loginUserPasswordInput)));
        passwordInputField.click();
        passwordInputField.sendKeys(password);
    }

    public static void setUserEmailInput(String email){
        WebElement userEmailInputField = webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(loginUserEmailInput)));
        userEmailInputField.click();
        userEmailInputField.sendKeys(email);
    }

    public static void setPasswordInput(String password){
        WebElement passwordInputField = webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(loginUserPasswordInput)));
        passwordInputField.click();
        passwordInputField.sendKeys(password);
    }

    public static void clickLogin(){
        WebElement loginButton = webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(loginUserButton)));
        loginButton.click();
    }

    public static void clickLogout(){
        WebElement logoutButton = webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(logoutUserButton)));
        logoutButton.click();
    }
}
