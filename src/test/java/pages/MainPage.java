package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static constants.Environment.*;
import static constants.XPath.*;
import static helpers.WebDriverHelper.getWaitWebDriver;
import static helpers.WebDriverHelper.getWebDriver;

public class MainPage {
    private static Boolean isInitialised = false;
    private static WebDriver webDriver = null;
    private static WebDriverWait webDriverWait = null;

    public static void setupMainPage() {
        if (!isInitialised) {
            webDriver = getWebDriver();
            webDriverWait = getWaitWebDriver();
            isInitialised = true;
        }
        if(dev.equalsIgnoreCase("true")){
            webDriver.get(devUrl + mainPageURL);
        } else {
            webDriver.get(prodUrl + mainPageURL);
        }
    }

    public static void navigateToMainPage(){
        if(dev.equalsIgnoreCase("true")){
            webDriver.get(devUrl + mainPageURL);
        } else {
            webDriver.get(prodUrl + mainPageURL);
        }
    }

    public static void refreshMainPage(){
        webDriver.navigate().refresh();
    }

    public static void tearDownMainPage() {
        webDriver.quit();
    }

    public static WebDriver getDriver(){
        return webDriver;
    }

    public static WebElement getCVButton(){
        return webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(cvDownloadButton)));
    }

    public static WebElement getMainPicture(){
        return webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(picture)));
    }

    public static WebElement getProjectTable(){
        return webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(projectsCardContainerTable)));
    }

    public static WebElement getMasteredTechnologiesTable(){
        return webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(masteredTechnologiesCardContainerTable)));
    }

    public static WebElement getLearningTechnologiesTable(){
        return webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(learningTechnologiesCardContainerTable)));
    }

    public static WebElement getProfessionalExperiencesContainer(){
        return webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(professionalExperiencesContainer)));
    }

    public static WebElement getFooter(){
        return webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(footerContainer)));
    }

    public static WebElement getDashboard(){
        return webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(dashboardContainer)));
    }

    public static WebElement getGraphsContainer(){
        return webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(dashboardGraphsContainer)));
    }

    public static WebElement getDashboardLogRepartitionGraph(){
        return webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(dashboardLogRepartitionGraph)));
    }

    public static WebElement getDashboardDailyAveragePerProjectGraph(){
        return webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(dashboardDailyAveragePerProjectGraph)));
    }

    public static WebElement getDashboardDailyAverageHistoryGraph(){
        return webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(dashboardDailyAverageHistoryGraph)));
    }

    public static WebElement getAddMasteredTechnologyButton(){
        return webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(masteredTechnologiesAddTechnologyButton)));
    }

    public static WebElement getDeleteMasteredTechnologyButton(){
        return webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(masteredTechnologiesDeleteTechnologyButton)));
    }

    public static WebElement getMasteredTechnologyNameInput(){
        return webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(masteredTechnologiesNameInput)));
    }

    public static WebElement getMasteredTechnologyDeleteInput(){
        return webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(masteredTechnologiesDeleteNameInput)));
    }

    public static WebElement getMasteredTechnologyUrlInput(){
        return webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(masteredTechnologiesUrlInput)));
    }

    public static WebElement getMasteredTechnologyIconUrlInput(){
        return webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(masteredTechnologiesIconUrlInput)));
    }

    public static WebElement getMasteredTechnologyCategoryInput(){
        return webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(masteredTechnologiesCategoryInput)));
    }

    public static WebElement getAddLearningTechnologyButton(){
        return webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(learningTechnologiesAddTechnologyButton)));
    }

    public static WebElement getDeleteLearningTechnologyButton(){
        return webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(learningTechnologiesDeleteTechnologyButton)));
    }

    public static WebElement getLearningTechnologyNameInput(){
        return webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(learningTechnologiesNameInput)));
    }

    public static WebElement getLearningTechnologyDeleteInput(){
        return webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(learningTechnologiesDeleteNameInput)));
    }

    public static WebElement getLearningTechnologyUrlInput(){
        return webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(learningTechnologiesUrlInput)));
    }

    public static WebElement getLearningTechnologyIconUrlInput(){
        return webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(learningTechnologiesUrlIconInput)));
    }

    public static WebElement getInsertTechnologyButton(){
        return webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(insertTechnologySendButton)));
    }

    public static WebElement getDeleteTechnologyButton(){
        return webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(deleteTechnologySendButton)));
    }

    public static void addMasteredTechnology(String technologyName, String url, String iconUrl, String category){
        getAddMasteredTechnologyButton().click();
        getMasteredTechnologyNameInput().sendKeys(technologyName);
        getMasteredTechnologyUrlInput().sendKeys(url);
        getMasteredTechnologyIconUrlInput().sendKeys(iconUrl);
        getMasteredTechnologyCategoryInput().sendKeys(category);
        getInsertTechnologyButton().click();
    }

    public static void addLearningTechnology(String technologyName, String url, String iconUrl){
        getAddLearningTechnologyButton().click();
        getLearningTechnologyNameInput().sendKeys(technologyName);
        getLearningTechnologyUrlInput().sendKeys(url);
        getLearningTechnologyIconUrlInput().sendKeys(iconUrl);
        getInsertTechnologyButton().click();
    }

    public static void deleteMasteredTechnology(String technologyName){
        getDeleteMasteredTechnologyButton().click();
        getMasteredTechnologyDeleteInput().sendKeys(technologyName);
        getDeleteTechnologyButton().click();
    }

    public static void deleteLearningTechnology(String technologyName){
        getDeleteLearningTechnologyButton().click();
        getLearningTechnologyDeleteInput().sendKeys(technologyName);
        getDeleteTechnologyButton().click();
    }

    public static WebElement getTechnologyByName(String technologyName){
        List<WebElement> technologies = webDriverWait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("[class^='Table_cellWithIcon_']")));
        int i;
        boolean found = false;
        for(i = 0; i < technologies.size(); i++){
            if(technologies.get(i).getText().equalsIgnoreCase(technologyName)){
                found = true;
                break;
            }
        }
        if(found){
            return technologies.get(i);
        }
        return null;
    }
}
