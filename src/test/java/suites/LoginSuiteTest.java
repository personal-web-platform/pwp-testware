package suites;

import org.junit.jupiter.api.*;
import org.openqa.selenium.html5.LocalStorage;

import static constants.Time.oneSecond;
import static constants.Time.fiveSecond;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static pages.LoginPage.*;

public class LoginSuiteTest {
    private static LocalStorage store;

    @BeforeAll
    public static void beforeAll() {
        setupLoginPage();
        store = getLocalStorage();
    }

    @BeforeEach
    public void beforeEach() {
        resetLocalStorage();
        navigateToLoginPage();
    }

    @AfterAll
    public static void afterAll() {
        tearDownLoginPage();
    }

    @Test
    @DisplayName("Check if correct user is able to login")
    void test01() throws InterruptedException {
        handleLogin();
        Thread.sleep(oneSecond);
        String authToken = store.getItem("token");
        String exp = store.getItem("exp");
        String iat = store.getItem("iat");
        String userEmail = store.getItem("mail");
        assertNotNull(authToken);
        assertNotNull(exp);
        assertNotNull(iat);
        assertNotNull(userEmail);
        assertFalse(authToken.isEmpty());
        assertFalse(exp.isEmpty());
        assertFalse(iat.isEmpty());
        assertFalse(userEmail.isEmpty());
    }

    @Test
    @DisplayName("Check if wrong user is not logged in")
    void test02() throws InterruptedException {
        handleLogin("random@test.com", "password");
        Thread.sleep(oneSecond);
        String authToken = store.getItem("token");
        String exp = store.getItem("exp");
        String iat = store.getItem("iat");
        String userEmail = store.getItem("mail");
        assertNull(authToken);
        assertNull(exp);
        assertNull(iat);
        assertNull(userEmail);
    }

    @Test
    @DisplayName("Check if user is able to logout")
    void test03() throws InterruptedException {
        handleLogin();
        Thread.sleep(oneSecond);
        handleLogout();
        Thread.sleep(fiveSecond);
        String authToken = store.getItem("token");
        String exp = store.getItem("exp");
        String iat = store.getItem("iat");
        String userEmail = store.getItem("mail");
        assertNull(authToken);
        assertNull(exp);
        assertNull(iat);
        assertNull(userEmail);
    }
}
