package suites;

import org.junit.jupiter.api.*;
import org.openqa.selenium.WebElement;

import static constants.Time.oneSecond;
import static constants.Time.twoSecond;
import static helpers.CacheHelper.waitForCacheResetAndRefreshPage;
import static org.junit.jupiter.api.Assertions.*;
import static pages.LoginPage.setupLoginPage;
import static pages.LoginPage.handleLogin;
import static pages.LoginPage.handleLogout;
import static pages.MainPage.*;

public class MainSuiteTest {
    private static boolean isLoggedIn = false;

    @BeforeAll
    public static void beforeAll() {
        setupMainPage();
    }

    @BeforeEach
    public void beforeEach() {
        navigateToMainPage();
    }

    @AfterEach
    public void afterEach() throws InterruptedException {
        if(isLoggedIn){
            Thread.sleep(oneSecond);
            handleLogout();
            Thread.sleep(oneSecond);
        }
    }

    @AfterAll
    public static void afterAll() {
        tearDownMainPage();
    }

    private static void handleLoggingIn() throws InterruptedException {
        setupLoginPage(getDriver());
        handleLogin(getDriver());
        Thread.sleep(twoSecond);
        isLoggedIn = true;
    }


    @Test
    @DisplayName("Check if main picture is displayed")
    void Test01() {
        assertTrue(getMainPicture().isDisplayed());
    }

    @Test
    @DisplayName("Check if download cv button is displayed")
    void Test02() {
        assertTrue(getCVButton().isDisplayed());
    }

    @Test
    @DisplayName("Check if project table is displayed")
    void Test03() throws InterruptedException {
        Thread.sleep(oneSecond);
        assertTrue(getProjectTable().isDisplayed());
    }

    @Test
    @DisplayName("Check if mastered technologies table is displayed")
    void Test04() throws InterruptedException {
        Thread.sleep(oneSecond);
        assertTrue(getMasteredTechnologiesTable().isDisplayed());
    }

    @Test
    @DisplayName("Check if learning technologies table is displayed")
    void Test05() throws InterruptedException {
        Thread.sleep(oneSecond);
        assertTrue(getLearningTechnologiesTable().isDisplayed());
    }

    @Test
    @DisplayName("Check if professional experiences container is displayed")
    void Test06() throws InterruptedException {
        Thread.sleep(oneSecond);
        assertTrue(getProfessionalExperiencesContainer().isDisplayed());
    }

    @Test
    @DisplayName("Check if footer table is displayed")
    void Test07() {
        assertTrue(getFooter().isDisplayed());
    }

    @Test
    @DisplayName("Check if dashboard is displayed")
    void Test08() throws InterruptedException {
        handleLoggingIn();
        assertTrue(getDashboard().isDisplayed());
    }

    @Test
    @DisplayName("Check if graphs are displayed")
    void Test09() throws InterruptedException {
        handleLoggingIn();
        WebElement GraphsContainer = getGraphsContainer();
        WebElement dashboardLogRepartitionGraph = getDashboardLogRepartitionGraph();
        WebElement dashboardDailyAveragePerProjectGraph = getDashboardDailyAveragePerProjectGraph();
        WebElement dashboardDailyAverageHistoryGraph = getDashboardDailyAverageHistoryGraph();
        assertTrue(GraphsContainer.isDisplayed());
        assertTrue(dashboardLogRepartitionGraph.isDisplayed());
        assertTrue(dashboardDailyAveragePerProjectGraph.isDisplayed());
        assertTrue(dashboardDailyAverageHistoryGraph.isDisplayed());
    }

    @Test
    @DisplayName("Check if able to add new mastered technology")
    void Test10() throws InterruptedException {
        handleLoggingIn();
        addMasteredTechnology("technology", "http://test.com", "http://test.com", "test");
        waitForCacheResetAndRefreshPage();
        WebElement masteredTechnology = getTechnologyByName("technology");
        assertNotNull(masteredTechnology);
        assertTrue(masteredTechnology.isDisplayed());
        deleteMasteredTechnology("technology");
        waitForCacheResetAndRefreshPage();
        WebElement deletedMasteredTechnology = getTechnologyByName("technology");
        assertNull(deletedMasteredTechnology);
    }

    @Test
    @DisplayName("Check if able to delete mastered technology - case insensitive")
    void Test11() throws InterruptedException {
        handleLoggingIn();
        addMasteredTechnology("technology2", "http://test.com", "http://test.com", "test");
        refreshMainPage();
        addMasteredTechnology("technology22", "http://test.com", "http://test.com", "test");
        waitForCacheResetAndRefreshPage();
        deleteMasteredTechnology("technology2");
        refreshMainPage();
        deleteMasteredTechnology("Technology22");
        waitForCacheResetAndRefreshPage();
        WebElement deletedMasteredTechnology1 = getTechnologyByName("technology2");
        WebElement deletedMasteredTechnology2 = getTechnologyByName("technology22");
        assertNull(deletedMasteredTechnology1);
        assertNull(deletedMasteredTechnology2);
    }

    @Test
    @DisplayName("Check if able to add new learning technology")
    void Test12() throws InterruptedException {
        handleLoggingIn();
        addLearningTechnology("technology3", "http://test.com", "http://test.com");
        waitForCacheResetAndRefreshPage();
        WebElement learningTechnology = getTechnologyByName("technology3");
        assertNotNull(learningTechnology);
        assertTrue(learningTechnology.isDisplayed());
        deleteLearningTechnology("technology3");
        waitForCacheResetAndRefreshPage();
        WebElement deletedMasteredTechnology = getTechnologyByName("technology3");
        assertNull(deletedMasteredTechnology);
    }

    @Test
    @DisplayName("Check if able to delete learning technology - case insensitive")
    void test13() throws InterruptedException {
        handleLoggingIn();
        addLearningTechnology("technology4", "http://test.com", "http://test.com");
        refreshMainPage();
        addLearningTechnology("technology44", "http://test.com", "http://test.com");
        waitForCacheResetAndRefreshPage();
        deleteLearningTechnology("technology4");
        refreshMainPage();
        deleteLearningTechnology("Technology44");
        waitForCacheResetAndRefreshPage();
        WebElement deletedMasteredTechnology1 = getTechnologyByName("technology4");
        WebElement deletedMasteredTechnology2 = getTechnologyByName("technology44");
        assertNull(deletedMasteredTechnology1);
        assertNull(deletedMasteredTechnology2);
    }
}
