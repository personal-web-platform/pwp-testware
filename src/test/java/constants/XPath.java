package constants;

public abstract class XPath {
    // AboutMe
    public static final String aboutMeContainer = "/html/body/div[1]/div/div/div[1]";
    public static final String cvDownloadButton = aboutMeContainer +"/div[1]/div[2]/div[1]/a";
    public static final String picture = aboutMeContainer +"/div[1]/div[2]/div[2]/div/div/img";

    // Projects
    public static final String projectsCardContainerTable = "/html/body/div[1]/div/div/div[2]/div/div/div/div/div/div/table";

    // Mastered technologies
    public static final String masteredTechnologiesCardContainerTable = "/html/body/div[1]/div/div/div[3]/div/div/div/div/div/div/table";
    public static final String masteredTechnologiesAddTechnologyButton = "/html/body/div[1]/div/div/div[4]/div/div/div/div/div/div[1]/button[2]";
    public static final String masteredTechnologiesDeleteTechnologyButton = "/html/body/div[1]/div/div/div[4]/div/div/div/div/div/div[1]/button[3]";
    public static final String masteredTechnologiesNameInput = "/html/body/div[2]/div/div/div[2]/div/form/input[1]";
    public static final String masteredTechnologiesUrlInput = "/html/body/div[2]/div/div/div[2]/div/form/input[2]";
    public static final String masteredTechnologiesIconUrlInput = "/html/body/div[2]/div/div/div[2]/div/form/input[3]";
    public static final String masteredTechnologiesCategoryInput = "/html/body/div[2]/div/div/div[2]/div/form/input[4]";
    public static final String masteredTechnologiesDeleteNameInput = "/html/body/div[2]/div/div/div[2]/div/div/input";

    // Learning technologies
    public static final String learningTechnologiesCardContainerTable = "/html/body/div[1]/div/div/div[4]/div[1]/div/div/div/div/div/table";
    public static final String learningTechnologiesAddTechnologyButton = "/html/body/div[1]/div/div/div[5]/div[1]/div/div/div/div/div[1]/button[2]";
    public static final String learningTechnologiesDeleteTechnologyButton = "/html/body/div[1]/div/div/div[5]/div[1]/div/div/div/div/div[1]/button[3]";
    public static final String learningTechnologiesNameInput = "/html/body/div[2]/div/div/div[2]/div/form/input[1]";
    public static final String learningTechnologiesUrlInput = "/html/body/div[2]/div/div/div[2]/div/form/input[2]";
    public static final String learningTechnologiesUrlIconInput = "/html/body/div[2]/div/div/div[2]/div/form/input[3]";
    public static final String learningTechnologiesDeleteNameInput = "/html/body/div[2]/div/div/div[2]/div/div/input";

    // Professional experiences
    public static final String professionalExperiencesContainer = "/html/body/div[1]/div/div/div[3]";

    // Footer
    public static final String footerContainer = "/html/body/div[1]/div/div/div[4]";

    // Login page
    public static final String loginUserEmailInput = "/html/body/div[1]/div/div/div/div/div/form/input[1]";
    public static final String loginUserPasswordInput = "/html/body/div[1]/div/div/div/div/div/form/input[2]";
    public static final String loginUserButton = "/html/body/div[1]/div/div/div/div/div/form/button";
    public static final String logoutUserButton = "/html/body/div[1]/div/div/div[1]/div[1]/div[1]/p";

    // Dashboard
    public static final String dashboardContainer = "/html/body/div[1]/div/div/div[2]/div/div[1]/div/div/div[2]";
    public static final String dashboardGraphsContainer = "/html/body/div[1]/div/div/div[2]/div/div[2]";
    public static final String dashboardLogRepartitionGraph = "/html/body/div[1]/div/div/div[2]/div/div[2]/div[1]/div/canvas";
    public static final String dashboardDailyAveragePerProjectGraph = "/html/body/div[1]/div/div/div[2]/div/div[2]/div[2]/div/canvas";
    public static final String dashboardDailyAverageHistoryGraph = "/html/body/div[1]/div/div/div[2]/div/div[2]/div[3]/div/canvas";

    // Modal
    public static final String insertTechnologySendButton = "/html/body/div[2]/div/div/div[2]/div/form/button";
    public static final String deleteTechnologySendButton = "/html/body/div[2]/div/div/div[2]/div/div/button";
}
