package constants;

public abstract class Environment {
    public static final String prodUrl = System.getenv("PRODURL");
    public static final String devUrl = System.getenv("DEVURL");
    public static final String dev = System.getenv("DEV");
    public static final String headless = System.getenv("HEADLESS");
    public static final String username = System.getenv("USERNAME");
    public static final String password = System.getenv("PASSWORD");

    // URLs
    public static final String mainPageURL = "/";
    public static final String loginPageURL = "/login";
}
