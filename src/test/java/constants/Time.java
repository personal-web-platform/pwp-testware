package constants;

public abstract class Time {
    public static final int oneSecond = 1000;
    public static final int twoSecond = 2000;
    public static final int fiveSecond = 5000;
}
