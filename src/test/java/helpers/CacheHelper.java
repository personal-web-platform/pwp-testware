package helpers;

import static constants.Time.twoSecond;
import static pages.MainPage.refreshMainPage;

public abstract class CacheHelper {
    public static void waitForCacheResetAndRefreshPage() throws InterruptedException {
        Thread.sleep(twoSecond);
        refreshMainPage();
        Thread.sleep(twoSecond);
    }
}
