# Pwp-testware

## Description

Testware library handling end-to-end testing for the personal web platform

## Technology stack

| Name             | Version |
|------------------|---------|
| OpenJDK          | 17.0.1  |
| Selenium         | 4.1.4   |
| Junit            | 5.8.2   |
| Webdrivermanager | 5.1.1   |
